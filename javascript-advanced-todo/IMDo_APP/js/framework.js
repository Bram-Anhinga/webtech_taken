/////\\\\\\\\\\\\\
//// FRAMEWORK \\\\
///\\\\\\\\\\\\\\\\\

function WrapperElement(el)
{
    // a wrapper element allow us to extend html dom functionality
    // without changing the behaviour of built-in elements
	
    // this contains the actual selection
    this.el = el;									
    
    // this allows us to see if a selection contains one or more elements
    if(el[0] != undefined)
    {
        this.isArray = true;
    }
    else
    {
        this.isArray = false;
    }
};

WrapperElement.prototype.toggleClass = function(className)
{
    if(this.isArray)
    {
		for(var i = 0; i<this.el.length; i++)
		{
			if(this.el[i].className.indexOf(className) == -1 )
			{
                this.el[i].className = this.el[i].className + " " + className;
            }
            else
            {
                this.el[i].className = this.el[i].className.substring(0, this.el[i].className.length - (className.length + 1));
            }   
		}
	}
	else
	{
		if(this.el.className.indexOf(className) == -1 )
		{
            this.el.className = this.el.className + " " + className;
        }
        else
        {
            this.el.className = this.el.className.substring(0, this.el.className.length - (className.length + 1));
        }
	}
	return this; 
};

WrapperElement.prototype.addClass = function(className)
{
	if(this.isArray)
	{
        // multiple elements, we'll need to loop
		for(var i = 0; i<this.el.length; i++)
		{
			this.el[i].className += " " + className;
		}
	}
	else
	{
        // just one element, so we can manipulate it without looping
		this.el.className = className;
	}
    
	return this;
    // return the original WrapperElement, so that we can chain multiple functions like
    // $("li").addClass("test").toggleClass("something");
};

WrapperElement.prototype.prepend = function(item)
{
	var newItem = this.el.firstChild;
	this.el.insertBefore(item, newItem);
};

WrapperElement.prototype.keyup = function(action){
    
	if(this.isArray)
	{
		// multiple elements, we'll need to loop
		for(var i = 0; i<this.el.length; i++)
		{
			this.el[i].addEventListener('keyup', action);
		}
	}
	else
	{
		// just one element, let's go nuts
		this.el.addEventListener('keyup', action);
	}
    
	return this;
};

WrapperElement.prototype.click = function(action)
{
    if(this.isArray)
	{
		for(var i=0; i<this.el.length; i++)
        {
            this.el[i].addEventListener("click", action);
        }
	}
	else
	{
		this.el.addEventListener("click", action);
	}
};

WrapperElement.prototype.val = function(value)
{
	if(value)
	{
        this.el.value = value;
    }
    else
    {
        return this.el.value;
    }
	
};

function $(selector)
{
	// check if selector is an object already e.g. by passing 'this' on clicks
	if(typeof(selector) == "object")
	{
		return new WrapperElement(selector);
	}

	// get the first character to know if we want an element, id or class
	var firstCharacter = selector.charAt(0);
	var selectedItems;

	switch(firstCharacter)
	{
		case '#':
		  var elementId = selector.substr(1);
		  selectedItems = document.getElementById(elementId);
		break;

		case '.':
		  var elementClass = selector.substr(1);
		  selectedItems = document.getElementsByClassName(elementClass);
		break;
		
		default:
            selectedItems = document.getElementsByTagName(selector);
        break;
	}

	var newElement = new WrapperElement(selectedItems);
	return newElement;
}