(function () {
    
    "use strict";
    
    var App, myApp, latitude, longitude;
    var weather = new Array();
    
    myApp = this;
    
    App = function () {
        
        this.showWheater = function () {
            
           var apiKey = "da8215b998571f21d1d7a56e4e15292e";
            
            var url = "https://api.forecast.io/forecast/" + apiKey + "/" + latitude + "," + longitude;
            
            console.log(url);
            
            $.ajax({
              type: 'GET',
              dataType: 'jsonp',
              url: url,
              success: function(data){
                  
                    var currentweather = data.currently;
                    var currentdate = new Date(); 
                  
                    console.log(currentweather);

                    localStorage.setItem("weather", JSON.stringify(currentweather));

                    weather['samenvatting'] = currentweather.summary;
                    weather['windsnelheid'] = currentweather.windSpeed * 3.6;
                    weather['bewolking'] = currentweather.cloudCover;
                    weather['temperatuur'] = (currentweather.temperature-32)/1.8;
                    weather['icoon'] = currentweather.icon;
                    weather['date'] = currentweather.time;
                    weather['neerslag'] = currentweather.precipIntensity;


                    $("#wind").html(Math.round(weather['windsnelheid']) + " km/u");
                    $("#curDeg").html(Math.round(weather['temperatuur']) + "<span class='deg'>&deg;</span>");
                    $("#summary").html(weather['samenvatting']);
                    $("#weathericon").html('<img class="weathericon" src="images/' + weather['icoon'] +'.png" alt="icon">');
                    $("#rain").html(weather['neerslag']*25.4 + ' mm');
                    $("#date").html(currentdate.getDate() + '/' + (currentdate.getMonth()+1)  + '/' + currentdate.getFullYear());
                  
                  
var datetime = "Now: " + currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/" 
            + currentdate.getFullYear() + " @ "  
            + currentdate.getHours() + ":"  
            + currentdate.getMinutes() + ":" 
            + currentdate.getSeconds();
                  
                  
                  
                   /*
                   var date = new Date(currentweather.time*1000);

                   var day = date.getDay();
                   console.log(date);
                    */



                },
                error: function () {

                }
            });
        };
    };

    
    // verkrijgen van de huidige positie van de gebruiker via geolocation
    function success(position) {
        
        var pos = position.coords;
        latitude = pos.latitude;
        longitude = pos.longitude;

        console.log('Your current position is:');
        console.log('Latitude : ' + latitude);
        console.log('Longitude: ' + longitude);

        myApp = new App();
        myApp.showWheater();
    };

    function error(err) {
      console.warn('ERROR(' + err.code + '): ' + err.message);
    };
        navigator.geolocation.getCurrentPosition(success, error);
    
}());